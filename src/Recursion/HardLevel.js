/**
 * write a function called reverse which accepts a string
 * and returns a new string in reverse manner
 */

// with helper function
function reverse(str) {
  let result = '';
  function helper(values) {
    if (!values) return '';
    else {
      result = result + values[values.length - 1];
    }
    helper(values.slice(0, values.length - 1))
  }
  helper(str);
  return result;
}
console.log(reverse("awesome")) // emosewa
console.log(reverse("rithmschool")) // loohcsmhtir

// without helper function
function reverse(str) {
  if (!str) return '';
  return str[str.length - 1] + reverse(str.slice(0, str.length - 1))
}
console.log(reverse("awesome")) // emosewa
console.log(reverse("rithmschool")) // loohcsmhtir

/**
 * write a recursive function called isPalindrome which returns true if string passed to it is palindrome
 * otherwise it returns false
 */


function isPalindrome(str) {
  if (str.length === 1) return true;
  if (str.length === 2) return str[0] === str[1];
  if (str[0] === str.slice(-1)) return isPalindrome(str.slice(1, -1))
  return false;
}

console.log(isPalindrome('awesome')) // false
console.log(isPalindrome('foobar')) // false
console.log(isPalindrome('tacocat')) // true
console.log(isPalindrome('amanaplanacanalpanama')) // true
console.log(isPalindrome('amanaplanacanalpandemonium')) // false

/**
 * flatten the array using recursion
 */
function flatten(values) {
  if (!values.length) return [];
  let results = [];

  for (let i = 0; i < values.length; i++) {
    if (Array.isArray(values[i])) {
      results = [...results, ...flatten(values[i])]
    }
    else {
      results.push(values[i]);
    }
  }

  return results;
}


console.log(flatten([1, 2, 3, [4, 5]])) // [1, 2, 3, 4, 5]
console.log(flatten([1, [2, [3, 4], [[5]]]])) // [1, 2, 3, 4, 5]
console.logflatten([[1], [2], [3]]) // [1,2,3]
console.logflatten([[[[1], [[[2]]], [[[[[[[3]]]]]]]]]]) // [1,2,3]

/**
 * write a recursive function called nestedEven sum that will return the sum 
 * of all the even numbers exist in an object in the whole nesting
 */

const isEven = val => val % 2 == 0;
// with helper
function nestedEvenSum(obj) {
  let sum = 0;
  function helper(hash) {
    //if(!(hash.keys.length)) return 0;

    for (let value in hash) {
      const currentValue = hash[value];

      if (typeof (currentValue) === "object" && !Array.isArray(currentValue)) {
        helper(hash[value])
      }
      else if (typeof (currentValue) === "number") {
        if (isEven(currentValue)) {
          sum = sum + currentValue;
        }
      }
    }
  }
  helper(obj);
  return sum;
}


var obj1 = {
  outer: 2,
  obj: {
    inner: 2,
    otherObj: {
      superInner: 2,
      notANumber: true,
      alsoNotANumber: "yup"
    }
  }
}

var obj2 = {
  a: 2,
  b: { b: 2, bb: { b: 3, bb: { b: 2 } } },
  c: { c: { c: 2 }, cc: 'ball', ccc: 5 },
  d: 1,
  e: { e: { e: 2 }, ee: 'car' }
};

console.log(nestedEvenSum(obj1)); // 6
console.log(nestedEvenSum(obj2)); // 10


/**
 * write a function called stringifyNumbers which takes in object and find all the
 * values which are number and converts them into string using recursion.
 */

function stringifyNumbers(obj) {
  var newObj = {};
  for (var key in obj) {
    if (typeof obj[key] === 'number') {
      newObj[key] = obj[key].toString();
    } else if (typeof obj[key] === 'object' && !Array.isArray(obj[key])) {
      newObj[key] = stringifyNumbers(obj[key]);
    } else {
      newObj[key] = obj[key];
    }
  }
  return newObj;
}



let obj = {
    num: 1,
    test: [],
    data: {
        val: 4,
        info: {
            isRight: true,
            random: 66
        }
    }
}
console.log(stringifyNumbers(obj))

/* output be like
{
    num: "1",
    test: [],
    data: {
        val: "4",
        info: {
            isRight: true,
            random: "66"
        }
    }
}
*/

/*
* write a function called collectStrings which accepts and object
* and returns all the strings that exist in object using recursion.
*/

const isString = (value) => typeof (value) === "string";

function collectStrings(obj) {
  let results = [];
  for (let key in obj) {
    if (isString(obj[key])) {
      results.push(obj[key]);
    }
    else if (typeof (obj[key]) === "object" && !Array.isArray(obj[key])) {
      results.push(...collectStrings(obj[key]));
    }
  }

  return results;
}


const obj = {
  stuff: "foo",
  data: {
      val: {
          thing: {
              info: "bar",
              moreInfo: {
                  evenMoreInfo: {
                      weMadeIt: "baz"
                  }
              }
          }
      }
  }
}

console.log(collectStrings(obj)) // ["foo", "bar", "baz"])