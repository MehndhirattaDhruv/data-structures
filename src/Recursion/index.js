// find odd number in array using recursion and helper function
// with helper function
function findOdds(arr) {
  let results = [];
  function checkOdd(values) {
    if (values.length === 0) return;
    if (values[0] % 2 != 0) results.push(values[0]);
    checkOdd(values.slice(1))
  }
  checkOdd(arr)
  return results;
}


// find odds with pure recursion without using the helper functions
function findOdds(arr) {
  let newArray = [];
  if (!arr.length) return [];
  if (arr[0] % 2 != 0) newArray.push(arr[0])
  return newArray.concat(findOdds(arr.slice(1)))
}

console.log(findOdds([1, 2, 3, 4, 5]))
/*
input
  [1].concat(findOdds([2,3,4,5]))
      [].concat(findOdds([3,4,5]))
          [3].concat(findOdds([4,5]))
              [].concat(findOdds([5]))
                [5].concat(findOdds([]))
                  []

output           [5].concat[]
                    [].concat[5]
                      [3].concat[5]
                        [].concat[3,5]
                          [1].concat[3,5]
                          [1,3,5]
*/

/******* end ***********/

// factorial using pure recursion
function factorial(num) {
  if (num === 0) return 1;
  return num * factorial(num - 1)
}
console.log(factorial(5));

/*
write a function called power which accepts a base and exponent. this function should return
the power of the base to the exponent. this function should mimic the functionality of
Math.pow(), don't worry about the negative base and exponents
*/
// approach 1 with helper function
function power(a, b) {
  let result = 1;
  function helper(base, exp) {
    if (exp === 0) return 1;
    else {
      result = result * base;
    }
    helper(base, exp - 1)
  }
  helper(a, b)
  return result
}

console.log(power(2, 0));
console.log(power(2, 1));
console.log(power(2, 4));

// approach 2nd using the pure recursion without helper function
function power(a, b) {
  if (b === 0) return 1;
  return a * power(a, b - 1);
}
console.log(power(2, 4));
/************ end ************/



/*
write a function which takes and array and
 gives the product of the array using recursion
*/
// 1st approach using helper function with helper function
function productOfArray(arr) {
  let result = 1;
  function helper(values){
    if(!values.length) return 1;
    result = result * values[0];
    helper(values.slice(1))
  }
  helper(arr);
  return result;
}

console.log(productOfArray([1,2,3])) // 6
console.log(productOfArray([1,2,3,10])) // 60

// 2nd approach without helper function and by using the pure recursive approach
function productOfArray(arr) {
  if(!arr.length) return 1;
  return arr[0] * productOfArray(arr.slice(1));
}

console.log(productOfArray([1,2,3])) // 6
console.log(productOfArray([1,2,3,10])) // 60

/*** end *****/

/**
 * write a function called recursiveRange which accepts a number
 * and add upto that number from 0 using recursion
 */
function recursiveRange(num){
  if(num === 0) return 0;
 return num + recursiveRange(num - 1);
}
console.log(recursiveRange(6)) // 0+1+2+3+4+5+6 = 21

console.log(recursiveRange(10))

/** end  */


/*
* write a function fib which accepts a number n and return nth number
in fibonocci series, recall the fibonicci it starts with 1,1,2,3,5,8...
*/

function fib(n){
  if(n === 0 || n === 1) return n;
 return fib(n-1) + fib(n-2);
}
// fib(4) // 3
// fib(10) // 55
// fib(28) // 317811
