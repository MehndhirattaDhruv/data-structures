/**
 * Sliding window pattern involves creating a window which can either be array
 * or number from one position to another, depending on a certain condition the window
 * either increases or closes and (a new window is created).
 * very useful for keeping track of a subset of data in array/string etc.
 * for example find longest sequence of unique character in "hellothere"
 * another example finding max sum from an array which accepts an array of integers with a number called n
 * the function calculate the maximum sum based on the consecutive no length provided by n.
 */

/*
write a function called maxSubarraySum which accepts a array of integers and
a number called n. The function should calculate the maximum sum of n consecutive elements in array.
console.log(maxSubarraySum([1,2,5,2,8,1,5], 2)) // 10
console.log(maxSubarraySum([1,2,5,2,8,1,5], 4)) // 17
console.log(maxSubarraySum([4,2,1,6], 1)) // 6
console.log(maxSubarraySum([], 4)) // null
*/

/*
naive approach
*/
function maxSubarraySum(arr, n) {
  let maxSum = null;
  for (let i = 0; i < arr.length - n + 1; i++) {
    let innerSum = 0;
    for (let j = i; j < i + n; j++) {
      innerSum = innerSum + arr[j];
    }
    if (innerSum > maxSum) {
      maxSum = innerSum;
    }
  }
  return maxSum;
}

console.log(maxSubarraySum([1, 2, 5, 2, 8, 1, 5], 2)); // 10
console.log(maxSubarraySum([1, 2, 5, 2, 8, 1, 5], 4)) // 17
console.log(maxSubarraySum([4, 2, 1, 6], 1)) // 6
console.log(maxSubarraySum([], 4)) // null

/**
 *this would work for small array and small n but what if we have very large array and high value of n
 and its O(n*n)
 */


/*
* second approach which will use only O(n) notation
* it will work on sliding window technique
* first it will hold the sum for the first n number in array
* then it will subtract the initial item and add the latest entry in the sum
* in this way it will iterate through each item once and fetch the desired results
*/
function maxSubarraySum(arr, n) {
  if (arr.length < n) return null;
  let maxSum = null;
  let tempSum = 0;
  for (let i = 0; i < n; i++) {
    tempSum = tempSum + arr[i];
  }
  maxSum = tempSum;
  for (let i = n; i < arr.length; i++) {
    tempSum = tempSum + arr[i] - arr[i - n];
    if (tempSum > maxSum) {
      maxSum = tempSum;
    }
  }
  return maxSum;
}
console.log(maxSubarraySum([1, 2, 5, 2, 8, 1, 5], 2)); // 10
console.log(maxSubarraySum([1, 2, 5, 2, 8, 1, 5], 4)) // 17
console.log(maxSubarraySum([4, 2, 1, 6], 1)) // 6
console.log(maxSubarraySum([], 4)) // null
console.log(maxSubarraySum([2, 3], 3)) // null


/**
 * Write a function called minSubArrayLen which accepts two parameters - an array of positive integers and a positive integer.
 * This function should return the minimal length of a contiguous subarray of which the sum
 *  is greater than or equal to the integer passed to the function. If there isn't one, return 0 instead
 */

// note we have to find the continuos array

function minSubArrayLen(arr, n) {

}
console.log(minSubArrayLen([2, 3, 1, 2, 4, 3], 7)) //2 because [4,3] makes and it its length is 2
console.log(minSubArrayLen([2, 1, 6, 5, 4], 9)) //2 because [5,4] makes and it its length is 2
console.log(minSubArrayLen([3, 1, 7, 11, 2, 9, 8, 21, 62, 33, 19], 52)) //1 because [62] is greater than 52 and its length is 1;
console.log(minSubArrayLen([1, 4, 16, 22, 5, 7, 8, 9, 10], 39)) //3 because [22,16, 4] is greater than 52 and its length is 1;



/**
 * find the function called longestSubstring which accepts a string and returns the length of
 * the longest substring with all distinct characters
 */
function longestSubstring(arr) {
  let result = {};
  let uniqueStr = '';
  let tempStr = '';
  let pos = 0;
  for (let i = 0; i < str.length; i++) {
    if (!result[str[i]]) {
      tempStr = tempStr + str[i];
      result[str[i]] = str[i];
    } else {
      i = pos;

      if (tempStr.length >= uniqueStr.length) {
        uniqueStr = tempStr;
      }
      pos++;
      tempStr = ''
      result = {};
    }
  }
  return uniqueStr.length;
}

console.log(longestSubstring('')) // 0
console.log(longestSubstring('thisisawesome')) // 6 awesom
console.log(longestSubstring('rithmschool')) // 7 rithms
console.log(longestSubstring('thecatinthehat')) // 7 hecatin
console.log(longestSubstring('bbbbb')) // 1 b
console.log(longestSubstring('longestsubstring')) // 7 longest

