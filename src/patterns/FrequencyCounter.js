/*
the frequenct counter uses the object or sets to collect values/frequencies of values
this can be used to avoid the O(n*n) complexity for operation on array or strings examples
are mentioned below
 */

/*
For example: write a function same which accepts two array as arguments
the function should return true if every value in the array has its
corresponding squared value in the second array.
the frequency of value must be same/order can vary
same([1,2,3] , [4,1,9])  // true
same([1,2,3], [1,9]) // false
same([1,2,1], [4,4,1]) // false(must use same frequency)
*/

/********  1st approach using O(n*n) ******/
function same(arr1, arr2) {
  if (arr1.length !== arr2.length) return false; // most common condition and basic condition to check

  for (let i = 0; i < arr1.length; i++) {
    let currentIndex = arr2.indexOf(arr1[i] * arr1[i]); // because of indexOf
    if (currentIndex === -1) return false;
    arr2.splice(currentIndex, 1)
  }
  return true
}

/******  2nd approach  using O(2n) which is O(n) notation approach *******/

/*
comparision of O(n*n) with O(2n) for n = 10
O(n*n)  = 100
O(2*n)  = 20
*/
function same(arr1, arr2) {
  if (arr1.length !== arr2.length) return false;
  let squaredResponse = {}
  for (let i = 0; i < arr1.length; i++) {
    const item = arr1[i];
    squaredResponse[item * item] = item;
  }
  for (let i = 0; i < arr2.length; i++) {
    if (!squaredResponse[arr2[i]]) return false;
    else {
      delete squaredResponse[arr2[i]]
    }
  }
  return true;
}



/******  3rd approach  using O(3n) which is O(n) notation approach *******/

function same(arr1, arr2) {
  if (arr1.length !== arr2.length) return false;
  const freqCounter1 = {}
  const freqCounter2 = {}

  for (let value of arr1) {
    freqCounter1[value] = (freqCounter1[value] || 0) + 1;
  }
  for (let value of arr2) {
    freqCounter2[value] = (freqCounter2[value] || 0) + 1;
  }
  // the two hashmap will hold the frequency of the item appearing in both the arrays

  for (let key in freqCounter1) {
    // key ** 2 helps in squaring an item and here key is the object key
    if (!freqCounter2[key ** 2]) return false; // if square of first array is not found then simply return false
    if (freqCounter2[key ** 2] !== freqCounter1[key]) return false; // checks if the frequency is matching or not
    // for example 2 is appearing 2 times so its square should also appear it same time

  }
  return true;
}

console.log(same([1, 2, 3, 2], [4, 1, 9, 4]));

/**
 * some more examples of the frequency counters
 * Anagrams - Given two strings write a function to determine if the
 * second string is anagram or not of the first
 * An anagram is a word, phrase or name formed by rearranging the letters
 * of another such as cinema formed by iceman
 */

const validAnagram = (str1, str2) => {
  if (!str1 && !str2) return true;
  let lookup = {}
  for (let value of str1) {
    lookup[value] = (lookup[value] || 0) + 1;
  }
  for (let value of str2) {
    if (!lookup[value]) {
      return false
    }
    else {
      lookup[value] -= 1;
    }
  }
  return true
}

console.log(validAnagram('', '')); // true
console.log(validAnagram('aaz', 'zza')); // false
console.log(validAnagram('rat', 'car')); // false
console.log(validAnagram('qwerty', 'qeywrt')); // true



/**
 * write a function sameFrequency. given two positive integers, find out if the two numbers
 * have same number of frequency of digits using O(n)
 * the solution can be multiple but i thought to add them and then compare them
 */
function sameFrequency(a, b) {
  let numA = String(a);
  let numB = String(b);
  if (numA.length !== numB.length) return false;
  let sumA = 0;
  let sumB = 0;
  for (let i = 0; i < numA.length; i++) {
    sumA = sumA + Number(numA[i]);
    sumB = sumB + Number(numB[i]);
  }
  return sumA === sumB
}

console.log(sameFrequency(182, 281)) // true
console.log(sameFrequency(34, 14)) //false
console.log(sameFrequency(3589578, 5879385)) //true
console.log(sameFrequency(22, 222)) //false




/**
 * implement a function called areThereDuplicates which accepts a variable
 * number of arguments and checks whether the arguments are duplicating or not
 * use either frequency counter or multiple pointer to solve the problem
 * time complexity should be O(n)
 */
function areThereDuplicates(...rest) {
  let result = {};
  for (let i = 0; i < rest.length; i++) {
    if (!result[rest[i]]) {
      result[rest[i]] = 1;
    }
    else {
      return true;
    }
  }
  return false;
}
console.log(areThereDuplicates(1, 2, 3)); //false
console.log(areThereDuplicates(1, 2, 2)); // true
console.log(areThereDuplicates('a', 'b', 'c', 'a')) //true


// fastest solution
function areThereDuplicates() {
  return new Set(arguments).size !== arguments.length;
}