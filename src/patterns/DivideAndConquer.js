/**
 * this pattern involves the dividing the data set into smaller chunks
 * and then repeating the process with a subset of data
 * this pattern can decrease the time complexity drastically.
 * an example is binary search where we are giving sorted array and then we need to find
 * the element which is expected and return its index or return -1 if not found.
 * search([1,2,3,4,5,7,9], 7) // 5
 * naive approach is O(n) to go through single element and then determine we call it as linear search
 */

/*
this solution will take O(log n) complexity very less than O(n)
because its skipping the array in chunks based on certain conditions
and will go through very specific conditions
*/
function binarySearch(arr, n) {
  let beg = 0;
  let end = arr.length - 1;
  while (beg <= end) {
    let mid = Math.floor((beg + end) / 2);
    if (n > arr[mid]) {
      beg = mid + 1;
    }
    else if (n < arr[mid]) {
      end = mid - 1;
    }
    else {
      return mid;
    }
  }
  return -1;
}

console.log(binarySearch([1, 4, 6, 8, 9, 11, 12, 15], 12)) // 6
console.log(binarySearch([1, 4, 6, 8, 9, 11, 12, 15], 17)) //  -1
console.log(binarySearch([1, 4, 6, 8, 9, 11, 12], 11)) // 5