/*
	Multiple pointers:-
	Creating pointers or values that corresponds to an index or position
	and move towards the beginning, end or middle based on certain conditions.
	Very efficient for solving problems with minimal space complexity as well.
	for example [-4,3,-2,1,2,3,4] when we try to find a pair we
	use one pointer from start and one from end towards middle just an example direction can be any
 */

/*
An example write a function called sumZero which accepts a sorted array
of integers. the function should return and find the first pair where sum is 0
Return and array that includes both values that sum to zero or undefined if such pair
doesn't exist.
*/
// conventional solution using O(n * n)
function sumZero(arr) {
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] + arr[j] === 0) {
        return [arr[i], arr[j]]
      }
    }
  }
}

console.log(sumZero([-3, -2, -1, 0, 1, 2, 3]))


// approach which use multiple pointers
function sumZero(arr) {
  let left = 0;
  let right = arr.length - 1;
  while (left < right) {
    let sum = arr[left] + arr[right];
    if (sum === 0) {
      return [arr[left], arr[right]]
    }
    else if (sum > 0) {
      right--;
    }
    else {
      left++;
    }
  }
}

console.log(sumZero([-4, -3, -2, -1, 0, 1, 2, 5]));
console.log(sumZero([0, 1, 2, 5]));


/*
Implement a function called countUniqueValues which accepts
a sorted array and counts the unique value in array and return the same
there can be negative numbers as well in the array, find the solution using the
multiple pointers in O(n), there can be others technique as well like hashmap and then return
the keys of hashmap
*/

/*
 In this we are moving j from second position to end in one loop
 for i we are moving it and replacing the next item with the unique no
 so we are only incrementing i if there is a unique value found from the previous one
 and then we are swapping it and at the end i pointer will let us know how many unique values are there
 from starting till it reached
 */
function countUniqueValues(arr) {
  let i = 0;
  for (let j = 1; j < arr.length; j++) {
    if (arr[i] !== arr[j]) {
      i++;
      arr[i] = arr[j];
    }
  }
  return i ? i + 1 : i;
}

console.log(countUniqueValues([1, 1, 1, 1, 2])) // output 2
console.log(countUniqueValues([1, 1, 1, 1, 2, 4, 5, 6, 6, 6, 7])) // output 6
console.log(countUniqueValues([])) // output 0
console.log(countUniqueValues([-2, -1, 0, 1, 1])) // output 4



/**
 * write a function averagePair. given a sorted array of integers and target average
 * determine if their is any pair of values in the array where the average of the pair
 * is equal to the target average. there might be more than one pair which exists
 *  which falls under this category it should have O(n) complexity only.
 */

function averagePair(arr, target) {
  let start = 0;
  let end = arr.length - 1;
  while (start < end) {
    let avg = (arr[start] + arr[end]) / 2;
    if (avg === target) return true;
    else if (avg < target) {
      start++;
    }
    else {
      end--;
    }
  }
  return false;
}

console.log(averagePair([1, 2, 3], 2.5))
console.log(averagePair([1, 3, 3, 5, 6, 7, 10, 12, 19], 8))

/**
 * Write a function called isSubsequence
 * which takes in two strings and checks whether the characters in the first string form a subsequence
 *  of the characters in the second string. In other words,
 *  the function should check whether the characters in the first string appear somewhere in the second
 *  string,without their order changing.
 * you can use the O(n+m) complexity.
 */
function isSubsequence(str1, str2) {
  let i = 0;
  let j = 0;
  while (j < str2.length) {
    if (str1[i] === str2[j]) {
      i++;
    }
    j++;
  }
  return i === str1.length ? true : false;

}
console.log(isSubsequence('hello', 'hello world')); //true
console.log(isSubsequence('sing', 'sting')); //true
console.log(isSubsequence('abc', 'abracadabra')); //true
console.log(isSubsequence('abc', 'acb')); //false order matters
