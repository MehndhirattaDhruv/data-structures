
/**
 * Biggest advantage is it adds more flexibility to go forward or backward in directions
 * drawback is its memory consumption is on the higher side because it has prev as well to maintain along with next
 */
class Node {
  constructor(val) {
    this.val = val;
    this.prev = null;
    this.next = null;
  }
}

class DoubleLinkedList {
  constructor() {
    this.length = 0;
    this.head = null;
    this.tail = null;
  }

  push(val) {
    const node = new Node(val);
    if (!this.head) {
      this.head = node;
      this.tail = this.head;
    } else {
      this.tail.next = node;  // 1-> 2
      node.prev = this.tail    // 1 <- 2
      this.tail = node; // make 2 as final tail
    }
    this.length++;
  }

  pop() {
    /**
     * advantage we don't need to loop through it like we did it in single linked list
     */
    let currentTail = this.tail;
    if (!currentTail) return undefined;
    if (this.length === 1) {
      this.head = null;
      this.tail = null;
    } else {
      this.tail = currentTail.prev;
      this.tail.next = null;
      currentTail.prev = null; // if we will not do this it will still hold the whole list using prev
      // basically it will still hold the references if we skip last line
    }

    this.length--;

    return currentTail;
  }

  shift(){
    // removes the first element from the linked list
    let oldHead = this.head;
    if(!oldHead) return undefined;
    if(this.length === 1){
      this.head = null;
      this.tail = null;
    }else {
      this.head = oldHead.next; //main part which will be doing the magic
      this.head.prev = null;
      oldHead.next = null; // for removing any forward references
    }
    this.length--;
    return oldHead;
  }

  unshift(val){
    let node = new Node(val);
    if(!this.head){
      this.tail = node;
      this.head = node;
    }else {
      this.head.prev = node;
      node.next = this.head;
      this.head = node;
    }
    this.length++;
    return this;
  }

  getItem(index){
    /**
     * biggest advantage is now we can loop from tail also using prev property
     * so if index is less than half of list length then we can start from head else
     * we can start from tail depending on index value passed
     * which makes searching is O(n/2) and we call it as O(n)
     */
    let counter;
    let currentHead = this.head;
    let currentTail = this.tail;
    if(index < 0 || index >= this.length) return null;
    else if(index < (this.length / 2)) {
      counter =0;
      while(counter !== index){
        currentHead = currentHead.next;
        counter++;
      }
      return currentHead;
    }else {
      counter = this.length -1;
      while(counter !== index){
        currentTail = currentTail.prev;
        counter--;
      }
      return currentTail;
    }
  }

  setItem(index, value){
    const node = this.getItem(index);
    if(node){
      node.val = value;
      return true;
    }
    return false;
  }
  insert(index, value){
    if(index < 0 || index > this.length) return false;
    else if(index === this.length) return !!this.push(value);
    else if(index === 0) return !!this.unshift(value);
    else {
      let prevNode = this.getItem(index-1);
      let newNode = new Node(value);
      let nextNode = prevNode.next;  // 1 2 lets say we want to insert 3
      prevNode.next = newNode;  // 1-> 3   2
      newNode.prev = prevNode; // 1 <-3
      newNode.next = nextNode; // 3-> 2
      nextNode.prev = newNode; // 1<-3< 2
    }
    this.length++;
    return this;
  }
  remove(index){
    if(index < 0 || index >= this.length) return false;
    else if(index === this.length -1) return !!this.pop();
    else if(index  === 0) return !!this.shift();

    let removedNode = this.getItem(index);  // A B C D lets say we want to delete C
    removedNode.prev.next = removedNode.next; //  A B-> D
    removedNode.next.prev = removedNode.prev; // A B <- D
    removedNode.prev = null; //cleaning the founded node
    removedNode.next = null;

    this.length--;
    return removedNode;
  }
}

const dl = new DoubleLinkedList();
dl.push(1);
dl.push(2);
dl.push(3);
dl.push(4);
console.log(dl);

// reverse a double linked list is pending from my side