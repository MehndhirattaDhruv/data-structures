class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
  }
}
// using node without LinkedList class and its a bad way of using linkedlist
// const node = new Node('hi');
// node.next = new Node('there');
// node.next.next = new Node('you')

// hi -> there -> you

class SinglyLinkedList {
  constructor() {
    this.head = null;
    this.tail = null;
    this.length = 0;
  }
  push(value) {
    //push by default insert at the end of the array or linked list
    const node = new Node(value);
    if (!this.head) {
      this.head = node;
      this.tail = node;
    } else {
      this.tail.next = node; // tail ke next ko different node dedi but abi tak pointer change ni hua its still head or the previous value
      this.tail = node; // tail ka pointer ab change hojaega is step se
    }
    this.length++;
    return this; // returning the whole list
  }

  unshift(val) {
    let current = this.head; // hold the current list in a variable
    const node = new Node(val);
    if (!current) {
      this.head = node;
      this.tail = node;
    } else {
      this.head = node; // head ko node dedi
      this.head.next = current; // uske baad uska next ko pura list se attach krdia
      /**
       * or we can use which is another way of doing that
       *  node.next = this.head;
       * this.head = node;
       */
    }
    this.length++;
    return this;
  }

  pop() {
    /* pseudo code which is always going to pop from the tail because pop works on on last node
      * if there is nothing returns undefined
      * set the next prop of 2nd last element to the desired value to be popped and set it to null and also its pointer
      * decrease length by 1 if element is popped and return the value of node which got removed
    */
    let currentNode = this.head;
    let previousNode = this.head;
    if (!currentNode) return undefined;

    while (currentNode.next) {
      previousNode = currentNode;
      currentNode = currentNode.next;
    }
    previousNode.next = null;
    this.tail = previousNode;
    this.length--;
    if (this.length == 0) {
      this.tail = null;
      this.head = null; // handles special case if item length is already one
    }

    return currentNode;
  }

  shift() {
    // remove the element from the start of the list i.e from the head
    let shiftedValue = this.head;
    if (!shiftedValue) return undefined;
    this.head = this.head.next;
    this.length--;
    return shiftedValue.val;
  }

  traverse() {
    let current = this.head;
    while (current) {
      console.log(current.val);
      current = current.next;
    }
  }

  getItem(position){
    /**
     * in this we need to iterate it through the position and return the item in it
     * because its don't have any built in index in it like arrays
     */
    if(position < 0 || position >= this.length) return null;
    let counter = 0;
    let current = this.head;
    while(counter !== position){
      current = current.next;
      counter++;
    }
    return current;
  }

  setItem(index, item){
  // simply set krna h item bs index pe fair and simple
    const nextNode = this.getItem(index); // ab jo node aegi item usse pele insert hoga because jo index h uspe hi insert krega na
    if(nextNode){
      nextNode.val = item;
      return true; // denote success
    }
    return false // for unsuccessful case
  }

  insertInto(index, item){
    // add an item to the particular index and push the subsequent elements forward direction
    if(index < 0 || index > this.length) return false;
    else if(index === this.length) return !!this.push(item); // to make it boolean plus it will cover the edge case
    else if(index === 0) return !!this.unshift(item);
    else {
      const node = new Node(item);
      let prevNode = this.getItem(index-1);
      let temp = prevNode.next;
      prevNode.next = node;
      node.next = temp;
      this.length++;
      return this;
    }
  }

  removeAt(index){
    if(index < 0 || index >= this.length) return false;
    if(index == 0) return !!this.shift();
    else if(index === this.length - 1) return !!this.pop();
    else {
      let prevNode = this.getItem(index - 1);
      let current = prevNode.next;
      prevNode.next = current.next;
      this.length--;
      return current;
    }
  }
  reverse(){
    // explaination in the red copy notes
    var node = this.head;
    this.head = this.tail;
    this.tail = node;
    var prev = null;
    var next;
    for(let i =0;  i < this.length; i++){
      next = node.next;
      node.next = prev;
      prev = node;
      node = next
    }
  }
}

const ll = new SinglyLinkedList();
ll.push(1);
ll.push(2);
ll.push(3);
ll.push(4);
console.log(ll)
