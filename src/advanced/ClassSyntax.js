/**
 * Javascript don't have inbuilt classes for linkedlist, graphs, trees
 * it has inbuilt class for array but thats more popular only because of that
 * reason they dispatch few inbuilt functions like push pop with it
 * A class is a blueprint for creating objects that comes with the predefined methods and properties
 * javascript don't have classes by default but they are special functions infact
 * js have not been using oops from starting they kind of used prototype based inheritance to enable the door of oops
 */

class Student{
  constructor(firstname, lastname) {
    this.firstname = firstname;
    this.lastname = lastname;
  }
  fullName() {
    return `your full name is ${this.firstname} ${this.lastname}`;
  }
  static subscribeStudents(...students) {
    // generally static methods are called without initiating the class and can be directly called
    // write a method to subscribe all the students
  }
}

const dhruv = new Student("dhruv", "mehndhiratta");
console.log(dhruv.fullName());

Student.subscribeStudents();