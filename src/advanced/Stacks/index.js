/**
 * the LIFO data structures
 * last item pushed going to be popped out first
 * used in managing functions invocations like in call stacks
 * used in undo/redo the last item changed can be undo or redo
 * routing history used stack
 * Insertion and deletion is O(1)
 * searching and accessing is  O(n)
 */

// array implementation
var stack = [];
stack.push("google.com");
stack.push("instagram.com");
stack.push("youtube.com");
console.log(stack);
stack.pop();
// using shift and unshift
stack = [];

stack.unshift("Create a new file");
stack.unshift("Create a new layout");
stack.unshift("Create a new design on top of it");
// now in order to remove we need to use shift
stack.shift();
console.log(stack)



// using linked list
/**
 * we can not use single linked list push and pop because
 * if you recall pop over there is O(n) and here we are expecting
 * both of them to be O(1)
 */


// using stack

class Node {
  constructor(val) {
    this.val = val;
    this.next = null;
  }
}
class Stack {
  constructor() {
    this.first = null;
    this.last = null;
    this.size = 0;
  }

  push(val) {
    const node = new Node(val);
    if (!this.first) {
      this.first = node;
      this.last = node;
    } else {
      node.next =  this.first;
      this.first = node;
    }
    return ++this.size;
  }
  pop() {
    if (!this.first) return null;
    var currentFirst = this.first;
    if (this.size === 1) {
      this.first = null;
      this.last = null;
    } else {
      this.first = this.first.next;
      currentFirst.next = null; // clearing out the references
    }
    this.size--;
    return currentFirst;
  }
}

const stack = new Stack();
stack.push(1);
stack.push(2);
stack.push(3);