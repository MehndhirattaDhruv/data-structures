// BFS traverses horizontally breadth wise until one level is completed it will not go to the next level
/*
this takes more space complexity than DFS because queue will grow along with it, and time complexity of both are same since we are 
traversing each node atleast one time
*/
class Node {
  constructor(val) {
    this.val = val;
    this.left = null;
    this.right = null;
  }
}

class BFS {
  constructor() {
    this.root = null;
  }
  insert(val) {
    const node = new Node(val);
    let current = this.root;
    if (!this.root) {
      this.root = node;
      return this;
    }
    else {
      while (true) {
        if (val === current.val) return undefined // denotes duplicate case
        if (val > current.val) {
          if (!current.right) {
            current.right = node;
            return this;
          } else {
            current = current.right;
          }
        } else {
          if (!current.left) {
            current.left = node;
            return this;
          } else {
            current = current.left;
          }
        }
      }
    }
  }
  search(val) {
    if (!this.root) return false; // root hi ni h ghanta pad lo
    else {
      let current = this.root;
      let found = false;
      while (current && !found) {// it will automatically comes out of loop if current is not having any value in it
        if (val < current.val) {
          current = current.left;
        } else if (val > current.val) {
          current = current.right;
        } else {
          // it means current has the value now and its equal
          found = true;
        }
      }
      if (!found) return "not found"
      return current;
    }
  }
  bfsTraverse() {
    let queue = [];
    let visited = [];
    let node = this.root
    queue.push(node);
    while (queue.length) {
      node = queue.shift();
      visited.push(node.val);
      if (node.left) {
        queue.push(node.left)
      }
      if (node.right) {
        queue.push(node.right)
      }
    }
    return visited
  }
}

const tree = new BFS();
tree.insert(10);
tree.insert(6);
tree.insert(15);
tree.insert(2);
tree.insert(4);
tree.insert(20);
tree.insert(12);


tree.bfsTraverse() //  10 6 15 4 20 2
/**
                10
            6         15
         4          12     20
      2
 */
