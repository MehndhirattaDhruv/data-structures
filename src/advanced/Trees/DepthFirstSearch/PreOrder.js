/**
 * All the DFS technique uses depth first
 * first grab the root node
 * grab the entire left side
 * then move to the right of it
 * Root -> Entire Left -> Entire Right
 */
class Node {
  constructor(val) {
    this.val = val;
    this.left = null;
    this.right = null;
  }
}

class PreOrder {
  constructor() {
    this.root = null;
  }
  insert(val) {
    const node = new Node(val);
    let current = this.root;
    if (!this.root) {
      this.root = node;
      return this;
    }
    else {
      while (true) {
        if (val === current.val) return undefined // denotes duplicate case
        if (val > current.val) {
          if (!current.right) {
            current.right = node;
            return this;
          } else {
            current = current.right;
          }
        } else {
          if (!current.left) {
            current.left = node;
            return this;
          } else {
            current = current.left;
          }
        }
      }
    }
  }
  search(val) {
    if (!this.root) return false; // root hi ni h ghanta pad lo
    else {
      let current = this.root;
      let found = false;
      while (current && !found) {// it will automatically comes out of loop if current is not having any value in it
        if (val < current.val) {
          current = current.left;
        } else if (val > current.val) {
          current = current.right;
        } else {
          // it means current has the value now and its equal
          found = true;
        }
      }
      if (!found) return "not found"
      return current;
    }
  }
  preOrderTraversel() {
    let visited = [];
    function recursiveHelper(node){
      visited.push(node.val);
      if(node.left) recursiveHelper(node.left)
      if(node.right) recursiveHelper(node.right)
    }
    recursiveHelper(this.root)
    return visited
  }
}

const tree = new PreOrder();
tree.insert(10);
tree.insert(6);
tree.insert(15);
tree.insert(3);
tree.insert(8);
tree.insert(20);

tree.preOrderTraversel() //  10 -> 6 -> 3 -> 8 -> 15 -> 20
/**
                10
            6           15
         3     8           20
 */