/**
 * All the DFS technique uses depth first
 * priority wise lowest left -> its node -> right if exist
 /*
  BFS takes more space complexity than DFS because queue will grow along with it, and time complexity of both are same since we are
  traversing each node atleast one time
 */
class Node {
  constructor(val) {
    this.val = val;
    this.left = null;
    this.right = null;
  }
}

class BreadthFirstSearch {
  constructor() {
    this.root = null;
  }
  insert(val) {
    const node = new Node(val);
    let current = this.root;
    if (!this.root) {
      this.root = node;
      return this;
    }
    else {
      while (true) {
        if (val === current.val) return undefined // denotes duplicate case
        if (val > current.val) {
          if (!current.right) {
            current.right = node;
            return this;
          } else {
            current = current.right;
          }
        } else {
          if (!current.left) {
            current.left = node;
            return this;
          } else {
            current = current.left;
          }
        }
      }
    }
  }
  search(val) {
    if (!this.root) return false; // root hi ni h ghanta pad lo
    else {
      let current = this.root;
      let found = false;
      while (current && !found) {// it will automatically comes out of loop if current is not having any value in it
        if (val < current.val) {
          current = current.left;
        } else if (val > current.val) {
          current = current.right;
        } else {
          // it means current has the value now and its equal
          found = true;
        }
      }
      if (!found) return "not found"
      return current;
    }
  }
  preOrderTraversel() {
    // we can also print pre order to check the difference in output
    let visited = [];
    function recursiveHelper(node){
      visited.push(node.val);
      if(node.left) recursiveHelper(node.left)
      if(node.right) recursiveHelper(node.right)
    }
    recursiveHelper(this.root)
    return visited
  }

  postOrderTraversel() {
    let visited = [];
    function recursiveHelper(node){
      if(node.left) recursiveHelper(node.left)
      if(node.right) recursiveHelper(node.right)
       visited.push(node.val);
    }
    recursiveHelper(this.root)
    return visited
  }
  inorderTraversel(){
    // priority wise lowest left -> its node -> right if exist
    let visited = [];
    function helperRecursive(node){
      if(node.left) helperRecursive(node.left)
      visited.push(node.val);
      if(node.right) helperRecursive(node.right)
    }
    helperRecursive(this.root);
    return visited
  }
}

const tree = new BreadthFirstSearch();
tree.insert(10);
tree.insert(6);
tree.insert(15);
tree.insert(3);
tree.insert(8);
tree.insert(20);

console.log(tree.preOrderTraversel()) // 10 -> 6 -> 3 -> 8 -> 15 -> 20 (root -> entire left -> entire right)
// ** analyze the preOrderTraversel output used where we want to clone/duplicate the tree
console.log(tree.postOrderTraversel()) // 3 -> 8 -> 6 -> 20 -> 15 ->10 (deepest left -> adjacent right -> root node)
console.log(tree.inorderTraversel()) //  3 -> 6 -> 8 -> 10 -> 15 -> 20 (deepest left -> root -> its right)
// ** in BST inorderTraversel gives sorted array by default
/**
                10
            6           15
         3     8           20
 */
