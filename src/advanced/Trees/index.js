/**
 * Lists are linear data structure means next to next to next in a line to find data there is only one path to traverse
 * whereas Trees are non linear and it can have multiple path ways
 * Root is a top node of the tree if we have two nodes at start its not a tree
 * child - a node directly connected to another node when moving away from the root node.
 * parent - vice versa definition of the child
 * siblings - group of nodes in the same parent level
 * leaf - a node with no children
 * edge - connection between the one node to another we can say its an arrow in simple terms
 * Advantages *****
 * used in HTML DOM
 * computer file folder system
 * json response is also have tree based architecture
 */