/**
 * here we are normally working on normal trees
 * for traversing a tree we have two ways:
 * 1) `breadth first` searching where we are traversing the tree breadth/level wise
 * for example
 *             10
 *        6          15
 *    3     8           20
 * output for this will be 10 -> 6 -> 15 -> 3 -> 8 -> 20     it goes breadth wise
 * 2) Depth first search
 * its also subcategorized into three types:
 * a) In order  3 -> 6 -> 8 -> 10 -> 15 -> 20 (deepest left -> root -> right) from above examples
 * b) Pre order  10 -> 6 -> 3 -> 8 -> 15 -> 20  (top root -> left -> right)
 * c) Post order 3 -> 8 -> 6 -> 20 -> 15 -> 10 (deepest left -> same level right -> root)
 * for example first it goes from 3 to 8 to 6 then it will try to find the left of 15 but there is no one so it picks right
 */

 /**
  * Advantages and disadvantages of both
  * the time complexity of DFS and BFS are same because we are visiting each node once
  * the space complexity in the BFS is more because they use queue to maintain the node, its left, right need to be popped off
  * the space complexity of DFS is less because they store the current whole left arm if we are processing the left arm using call stack
  */