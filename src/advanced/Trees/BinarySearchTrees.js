/**
 * each node can have max 2 nodes
 * it can one or zero node in its child also thats also valid but not more than 2
 * binary search tree have one special property that in each node the left side is less than the root node value
 * and the right side is greater than the root node value
 * for example check 10 left side of 10 is always less than 10 and right side is always greater than 10
 * for example check 15 its left don't have anything but 20 is greater than it
 * its useful lookups because of the ways its sorted and its easy to find that where the node is desired to be get added
 * insertion and searching is O(log n) because in each iteration it skips the half part like it skip the whole left part if item is greater than root

                 10
              6       15
           4              20
         2
 */

class Node {
  constructor(val) {
    this.val = val;
    this.left = null;
    this.right = null;
  }
}

class BinarySearchTree {
  constructor() {
    this.root = null;
  }
  insert(val) {
    const node = new Node(val);
    let current = this.root;
    if (!this.root) {
      this.root = node;
      return this;
    }
    else {
      while (true) {
        if (val === current.val) return undefined // denotes duplicate case
        if (val > current.val) {
          if (!current.right) {
            current.right = node;
            return this;
          } else {
            current = current.right;
          }
        } else {
          if (!current.left) {
            current.left = node;
            return this;
          } else {
            current = current.left;
          }
        }
      }
    }
  }
  search(val) {
    if (!this.root) return false; // root hi ni h ghanta pad lo
    else {
      let current = this.root;
      let found = false;
      while (current && !found) {// it will automatically comes out of loop if current is not having any value in it
        if (val < current.val) {
          current = current.left;
        } else if (val > current.val) {
          current = current.right;
        } else {
          // it means current has the value now and its equal
          found = true;
        }
      }
      if(!found) return "not found"
      return current;
    }
  }
}

const tree = new BinarySearchTree();
tree.insert(10);
tree.insert(6);
tree.insert(15);
tree.insert(4);
tree.insert(2);
tree.insert(20);
//console.log(tree)
console.log(tree.search(10));
/* conventional way of insertion
tree.root = new Node(10);
tree.root.left = new Node(5);
tree.root.right = new Node(20);
tree.root.left.right = new Node(7);
*/
