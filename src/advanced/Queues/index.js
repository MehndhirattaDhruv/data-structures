/**
 * Queues are first in first out FIFO
 * example print task queue execution / task processing
 * if we use array implementation is using O(n) for implementation
 */

// using array
var queue = [];
queue.push(1);
queue.push(12);
queue.push(15);

queue.shift();
console.log(queue)
// vice versa

queue = [];
queue.unshift(1);
queue.unshift(12);
queue.unshift(15);

queue.pop();
console.log(queue);

// using linked list
/**
 * here push is called enqueue and pop is called dequeue
 * so for insertion we will use push and for deletion we will use shift kind of arrangement
 * if we go with other pop technique then it will have O(n) complexity
 */



class Node {
  constructor(val) {
    this.val = val;
    this.next = null
  }
}

class Queue {
  constructor() {
    this.first = null;
    this.last = null;
    this.size = 0
  }
  enqueue(val) {
    const node = new Node(val);
    if (!this.first) {
      this.first = node;
      this.last = node;
    } else {
      this.last.next = node;
      this.last = node;
    }
    ++this.size;
    return this;
  }
  dequeue() {
    // same as the stack pop because we are removing it from first
    if (!this.first) return null;
    var currentFirst = this.first;
    if (this.size === 1) {
      this.first = null;
      this.last = null;
    } else {
      this.first = this.first.next;
      currentFirst.next = null; // clearing out the references
    }
    this.size--;
    return currentFirst;
  }
}

const qu = new Queue();
qu.enqueue(1);