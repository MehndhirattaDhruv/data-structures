// 1st type

console.log([] + []);
/*
'' basically its doing type casting of string because we are using
 concatenation sign and empty array gets converted to empty string and in
  this case its adding "" +  "" = ""
*/

console.log({}+[])
// output socho khud


// 2nd Type
function a(){
  return 'hello';
}

const sentence = a `sing`;

console.log(sentence);
/* Output:  socho khud
here sing acting as a tagged template  if we have any tagged template next to function then it become its argument
it will act as a('sing') since our function is not handling argument here in this case
but will show and example in the next line
 */

function taggedTemplate(name){
  return 'hello' + ' ' + name;
}

const res = taggedTemplate `sing`;

console.log(res);
// Output: socho khud


// 3rd Type
/* if we want to make content editable of a whole websote just add following in the console then see the magic
document.body.contentEditable = true;
*/

// 4th Type

function y() {
  console.log(this.length);
}

var x = {
  length: 5,
  method: function(y) {
    arguments[0]()
  }
}

x.method(y, 1)
/*
Output:
because we are passing two arguments to method function and it
 calls the y function because y is its first argument but in that function this refers to the no. of arguments passed
 which is 2 so thats why output is also 2
*/

// 5th Type
const x = 'constructor'
console.log(x[x](01));

/* basically when we are trying to access x['constructor'] or x.constructor we are getting a native function of String class
and in that we have this function the declaration of x also can be understand such as
const x = new String('constructor')
so that native function will return whatever it takes in this case it took 01 and returns 1 as output

*/

// 6th type
// return length of arguments passed to a function without using loops

let a = function() {
  return [].slice.call(arguments).length;
}
console.log(a(1,2,3,4,5))