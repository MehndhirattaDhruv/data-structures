

/* Usage of fat arrow function */

const profile = {
  firstName: '',
  lastName: '',
  setName: function (name) {
    let splitName = function (n) {
      let fullname = n.split(' ');
      this.firstName = fullname[0];
      this.firstName = fullname[1];
    }
    splitName(name);
  }
}

profile.setName('john walker');
console.log(profile.firstName); // undefined

console.log(window.firstName); // john

/* firstName is undefined in this case but if we will switch it to window.firstName
 it will give desirable result the reason behind its setting value to the window object
*/

const prof = {
  firstName: '',
  lastName: '',
  setName: function (name) {
    let splitName = (n) => {
      let fullname = n.split(' ');
      this.firstName = fullname[0];
      this.lastName = fullname[1];
    }
    splitName(name);
  }
}

prof.setName('john walker');
console.log(prof.firstName); //john

/* after use of fat arrow we don't need to use the window object the reason behind that fat arrown function
 doesn't have its own this its representing the this of its parent and in our case its profile object 
*/



/* Proptypal inheritance:  basically suppose there are thousands of objects
then they don't need to have each propeerty defined in themselves
 they may get it from its parent from where we are inheriting the
  object that is the reason why objects are light weight
 */

let car = function (model) {
  this.model = model; // this is just acting as a constructor
}

car.prototype.getModel = function () {
  return this.model // here we are updating the prototype of car object
}

const toyota = new car('toyota');
toyota.getModel()

const nissan = new car('nissan');
nissan.getModel()



/* Closures: basically when a function is called inside a function and that inner function
 is holding its outer environment and the required values which are going to be used in its inner function
 */

let obj = function () {
  let i = 0;
  return {
    setI(k) {
      i = k;
    },
    getI() {
      return i;
    }
  }
}

let res = obj();
res.setI(2);
console.log(res.getI());