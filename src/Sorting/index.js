// Go for visiual go website for visual visualization of any sorting technique if we got confused anywhere
/**
 * built in array sort method
 * but this not works well always suppose for alphabets its sorts from a-z by default and for
 * numbers it sorts from highest to lowest by default which is totally different and opposite in both the cases.
 * in order to make them properly work we can instruct javascript to use comparator
 * if we return a negative number it means a should come before b
 * if we return a positive number it means a should come after b
 * if we return  0 it means both are same
 */

 function numberCompare(num1, num2){
   return num1 - num2; // if we change it to num2 - num1 will change order
 }
[6,2,4,1,9].sort(numberCompare) // 1,2,4,6,9

/**
 * selection sort
 * same complexity O(n*n)
 */

function selectionSort(values) {
  for(let i = 0; i < values.length -1; i++) {
    for(let j = i ; j < values.length; j++){
      if(values[i] > values[j]) {
        [values[j],values[i]] = [values[i],values[j]]; //es5 way of swapping
      }
    }
  }

  return values;
}
selectionSort([8,2,3,1,4])


/**
 * bubble sort
 * it works well for almost sorted array which have lets say one element unsorted
 * time complexity is O(n*n)
 * and if data is already sorted then use extra variable called noSwap and if no swap is happening
 * inside the j loop break it out through it and it has time complexity of O(n)
 */
function bubbleSort(values) {
  for(let i = 0; i < values.length -1; i++) {
    for(let j = 0 ; j < values.length - i; j++){
      if(values[j] > values[j +1 ]) {
        [values[j+1], values[j]]= [values[j], values[j+1]]
      }
    }
  }

  return values;
}
bubbleSort([8,2,3,1,4])

/**
 * insertion sort also have O(n*n) complexity
 * it treats its left side from pointer as sorted and check where the current element
 * can be placed by going through all the elements in the left side which it treats as sorted and try to find the spot
 */
[5,3,4,1,2]
   |
[3,5,4,1,2]
     |
[3,4,5,1,2]
       |
[1,3,4,5,2]
         |
[1,2,3,4,5]
function insertionSort(arr) {
  //starting from 1 because we considered the starting element is already sorted
  for(var i = 1; i< arr.length; i++) {
    var currentValue = arr[i];
    for(var j = i - 1; j >= 0 && arr[j] > currentValue; j--){
     arr[j+1] = arr[j]
    }
    arr[j+1] = currentValue
  }
    return arr;
}
insertionSort([5, 3, 4, 1, 2])