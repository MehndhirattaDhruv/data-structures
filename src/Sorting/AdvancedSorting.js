/**
 * Merge Sort
 * most of the algos in this section are going to have time complexity of O(n * log n)
 * it has space complexity of O(n) and all the fundamental sorting like bubble has space complexity of O(1) and this is drawback of merge sort
 * merge sort is a combination of splitting, merging, and sorting
 * worked on decomposing the array into smaller section then bundled them upto a larger array and used divide and conquer
 * it takes advantage of the fact one array is having o or 1 element we know that its already sorted.
 */


/*
create a program which will be merging the two sorted arrays and going
to act as a helper function in merge sort
*/

/*
[8,3,5,4,7,6,1,2]

[8,3,5,4] [7,6,1,2]

[8,3] [5,4] [7,6] [1,2]

[8] [3] [5] [4] [7] [6] [1] [2]  // decomposition point till it takes O(log n )
                                   complexity because for 8 elements it takes 3 steps to decompose which is 2's power 3 = 8
  and then n adds to log n because we are comparing 4 and 4 elements in mergeSortedArrays function using O(n) complexity
   overall it takes O(n log n)
[3 ,8] [4, 5] [6, 7] [1,2]

[3,4,5,8] [1,2,6,7]

[1,2,3,4,5,6,7,8]

*/
const mergeSortedArrays = (arr1, arr2) => {
  let results = [];
  let ptr = 0;
  for (let i = 0; i < arr1.length; i++) {
  if (arr2[ptr] === undefined || arr1[i] < arr2[ptr]) {
      results.push(arr1[i]);
  } else {
      results.push(arr2[ptr]);
      ptr++;
      i = i - 1;
    }
  }

  return ptr === arr2.length ? results : [...results, ...arr2.slice(ptr)];
}


function mergeSort(arr) {
  console.log(arr, 'arr')
  if(arr.length <= 1) return arr;
  let midPoint = Math.floor((arr.length/2));
  let left = mergeSort(arr.slice(0, midPoint)); // 4 -> 2 -> 1
  let right = mergeSort(arr.slice(midPoint)); // 4
 return mergeSortedArrays(left, right);
}
console.log(mergeSort([8,3,5,4,7,6,1, 2]), "output")
console.log(mergeSort([8,3,5,4]), "output")
/**
 *first it will divide the array into 2 and 2 chunk [8,3] [5,4]
 left = [8,3] but it will keep this calling until the base condition is hit
    left =  [8] it will return
    right = [3] it will also return mergeSortedArrays and output will be [3,8]
    now they will call the function
 right = [5,4]
 *    left = [5]
      right = [4] it will also return mergeSortedArrays and output will be [4,5]
      but after the call stack which is remaining i.e the last left and right which initially got divided need to be on callstack
      then it will call mergeSortedArrays with [3,8]  [4,5]
 */
///  merge sort ends /////



/**  Quick Sort
 * In this we pick any element as pivot element and we move the numbers lesser to it
 * to its left and greater than pivot to the right.
 [5,2,1,8,4,7,6,3]
  | pick 5 as pivot and determine how many elements are lesser than that and move them around
  3, 2, 1, 4, 5, 7, 6, 8 now 4 is at correct place we need to repeat the same process recrusively for the left side of 4 and right side of 4
  3,2,1 pick 3 as pivot and determine where it can comes
  2,1,3,4,5,7,6,8
  again pick 2 as pivot and move them around
  1,2,3,4,5,7,6,8 now do the same kind of process on the right side.
 */

// for more information checkout basics folder
function quickSort(arr) {
  if (arr.length == 1) return arr;

  const leftArr = [];
  const rightArr = [];
  const pivot = arr[arr.length - 1];

  for (let i = 0; i < arr.length - 1; i++) {
    if (arr[i] < pivot)
      leftArr.push(arr[i]);
    else
      rightArr.push(arr[i]);
  }

  if (leftArr.length && rightArr.length) {
    return [...quickSort(leftArr), pivot, ...quickSort(rightArr)]
  }
  else if (leftArr.length) {
    return [...quickSort(leftArr), pivot]
  }
  return [pivot, ...quickSort(rightArr)];
}


console.log(quickSort([51, 1, 23, 5, 1, 55, 12, 13, 71, 9, 6]))