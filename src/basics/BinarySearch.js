/*
It finds the location of an element in a sorted array using the beginning,
 end, and mid approach  if the item which needs to be found is greater than mid element then
  we can change beg to mid+1 and in else case we can change the end to mid-1
*/

function binarySort(arr, item) {
  let beg = 0;
  let end = arr.length;
  let mid = Math.floor((beg + end) / 2);

  let pos;
  while (beg <= end || arr[mid] != item) {
    mid = Math.floor((beg + end) / 2);
    if (item > arr[mid]) {
      beg = mid + 1;
    }
    else {
      end = mid - 1;
    }
    if (arr[mid] == item) {
      pos = mid;
    }
  }
  //console.log(beg, mid, end)
  return mid;
}

console.log(binarySort([1, 4, 6, 19, 21, 27], 21));
