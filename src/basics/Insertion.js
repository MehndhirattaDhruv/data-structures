function insert(arr, item, position) {
  for (let i = arr.length - 1; i >= position; i--) {
    arr[i + 1] = arr[i];
    if (i == position) {
      arr[i] = item;
    }
  }
  return arr;
};

console.log(insert([1, 5, 6, 8, 11], 15, 2), 'function call');