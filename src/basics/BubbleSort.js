/*
Bubble sort helps in sorting of elements

Eg it will go through 3 passes only because its length is 4

Pass 1:
10       9      9    9
9         10    7    7
7         7     10   4
4         4      4    10


Pass  2:
9       7      7
7       9      4
4       4      9
10     10    10

*/

function bubbleSort(arr) {
  let temp;
  for (let i = 0; i < arr.length - 1; i++) {
    for (let j = 0; j < arr.length - 1 - i; j++) {
      if (arr[j] > arr[j + 1]) {
        temp = arr[j + 1];
        arr[j + 1] = arr[j];
        arr[j] = temp;
      }
    }
  }
  return arr;
}

console.log(bubbleSort([6, 1, 5, 8]))
