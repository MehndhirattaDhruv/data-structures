/*
Pass 1:
10       9      7       4
9         10    10      10
7         7       9      9
4         4      4       7


Pass  2:
4       4     4     
10     9     7       
9       10   10    
7        7     9      

*/

function selectionSort(arr) {
  let temp;
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] > arr[j]) {
        temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
      }
    }
  }
  return arr;
}

console.log(selectionSort([6, 1, 5, 8, 2]))