
/* this function call itself recursively for divide and conquer basically it will select any element as the pivot element
  and then it will create 2 sub-arrays in which the first will be holding values less than that pivot and the second array
  holding values larger than the pivot and it will call it recursively until array length is one or single element remains in that array.
*/

function quickSort(arr) {
  if (arr.length == 1) return arr;

  const leftArr = [];
  const rightArr = [];
  const pivot = arr[arr.length - 1];

  for (let i = 0; i < arr.length - 1; i++) {
    if (arr[i] < pivot)
      leftArr.push(arr[i]);
    else
      rightArr.push(arr[i]);
  }

  if (leftArr.length && rightArr.length) {
    return [...quickSort(leftArr), pivot, ...quickSort(rightArr)]
  }
  else if (leftArr.length) {
    return [...quickSort(leftArr), pivot]
  }
  return [pivot, ...quickSort(rightArr)];
}


console.log(quickSort([51, 1, 23, 5, 1, 55, 12, 13, 71, 9, 6]))
