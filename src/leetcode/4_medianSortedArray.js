/*
Mean = no. of elements total sum/np of elements count
Median = center element from the array or from the list

- if list length is odd: center element called as median
- if list length is even: pick two center and then divide it by 2

Input: ar1[] = {-5, 3, 6, 12, 15}
        ar2[] = {-12, -10, -6, -3, 4, 10}
Output : The median is 3.
Explanation : The merged array is :
        ar3[] = {-12, -10, -6, -5 , -3,
                 3, 4, 6, 10, 12, 15},
       So the median of the merged array is 3

Input: ar1[] = {2, 3, 5, 8}
        ar2[] = {10, 12, 14, 16, 18, 20}
Output : The median is 12.
Explanation : The merged array is :
        ar3[] = {2, 3, 5, 8, 10, 12, 14, 16, 18, 20}
        if the number of the elements are even,
        so there are two middle elements,
        take the average between the two :
        (10 + 12) / 2 = 11.
*/

function medianSortedArray(arr1, arr2) {
  const finalArr = arr1.concat(arr2).sort(function (a, b) {
    return a - b;
  });
  if (!(finalArr.length % 2 === 0)) {
    return finalArr[Math.floor(finalArr.length / 2)]
  }

  else {
    return (finalArr[(finalArr.length / 2) - 1] + finalArr[(finalArr.length / 2)]) / 2;
  }
}

console.log(medianSortedArray([-5, 3, 6, 12, 15], [-12, -10, -6, -3, 4, 10])); // 3

console.log(medianSortedArray([2, 3, 5, 8], [10, 12, 14, 16, 18, 20])) // 11