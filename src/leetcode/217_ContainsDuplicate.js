/**
Given an integer array nums, return true if any value appears at least twice in the array,
 and return false if every element is distinct.


Example 1:

Input: nums = [1,2,3,1]
Output: true
Example 2:

Input: nums = [1,2,3,4]
Output: false
Example 3:

Input: nums = [1,1,1,3,3,4,3,2,4,2]
Output: true

approach  -> hash bnake check krlia kisi be key ki value 1 se upar toh in hai
*/

/**
 * @param {number[]} nums
 * @return {boolean}
 */
 var containsDuplicate = function(nums) {
  let hash = {}
  for(let i = 0; i < nums.length; i++){
      if(!hash[nums[i]]) hash[nums[i]] = 0;
      ++hash[nums[i]];
  }
  return !Object.values(hash).every(item => item === 1)

};