/**

You are given an integer array height of length n. There are n vertical lines drawn such that
 the two endpoints of the ith line are (i, 0) and (i, height[i]).

Find two lines that together with the x-axis form a container, such that the container contains the most water.

Return the maximum amount of water a container can store.

Notice that you may not slant the container.

Input: height = [1,8,6,2,5,4,8,3,7]
Output: 49
Explanation: The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7]. In this case, the max area of water (blue section) the cont

Example 2:

Input: height = [1,1]
Output: 1

approach take two pointers -> left and right shrink both of them based on some logic conditions
check in which argument we are getting max area
*/

/**
 * @param {number[]} height
 * @return {number}
 */

function fetchArea(x1, y1, x2, y2) {
  return (x2 - x1) * Math.min(y1, y2);
}
var maxArea = function (height) {
  let maxValue = 0;
  let rightPtr = height.length - 1;
  let leftPtr = 0;
  while (leftPtr < rightPtr) {
    maxValue = Math.max(
      maxValue,
      fetchArea(leftPtr, height[leftPtr], rightPtr, height[rightPtr])
    );

    if (height[leftPtr] > height[rightPtr]) {
      rightPtr--;
    } else {
      leftPtr++;
    }
  }

  return maxValue;
};
