/*
There is a robot on an m x n grid. The robot is initially located at the top-left corner (i.e., grid[0][0]). The robot tries to move to the bottom-right corner (i.e., grid[m - 1][n - 1]). The robot can only move either down or right at any point in time.

Given the two integers m and n, return the number of possible unique paths that the robot can take to reach the bottom-right corner.

The test cases are generated so that the answer will be less than or equal to 2 * 109.
Input: m = 3, n = 2
Output: 3
Explanation: From the top-left corner, there are a total of 3 ways to reach the bottom-right corner:
1. Right -> Down -> Down
2. Down -> Down -> Right
3. Down -> Right -> Down

Input: m = 3, n = 7
Output: 28

let say m = 2 n =2
1st approach
-> -> chala right maara fir down maara
    |
2nd approach
->
->  | chala down fir right har step mein usse 1 step toh lena pdra hai hai toh initialize 1 se krdo
for diagonal values ko plus krte chlo
*/


/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
 var uniquePaths = function(m, n) {
  let arr = [...new Array(m)]
      .map((item, i) => [...new Array(n)]
          .map((item, index) => index === 0 || i === 0 ? 1 : 0));
  for(let i = 0; i < arr.length; i++){
      for(j = 0; j < arr[0].length; j++){
         if(arr[i][j] !== 1){
             arr[i][j] = arr[i-1][j] + arr[i][j -1]
         }
      }
  }
 return arr[m-1][n-1]
};