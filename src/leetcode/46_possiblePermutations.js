/*
Given an array nums of distinct integers, return all the possible permutations. You can return the answer in any order.
Input: nums = [1,2,3]
Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]

Input: nums = [0,1]
Output: [[0,1],[1,0]]

Input: nums = [1]
Output: [[1]]
*/


let findPermutations = (string) => {
  if (string.length < 2 ){
    return string
  }
  let permutationsArray = []

  for (let i = 0; i < string.length; i++){
    let char = string[i]

    if (string.indexOf(char) != i)
    continue

    let remainingChars = string.slice(0, i) + string.slice(i + 1, string.length) //bc

    for (let permutation of findPermutations(remainingChars)){
      permutationsArray.push(char + permutation) // abc
    }
  }
  return permutationsArray
}