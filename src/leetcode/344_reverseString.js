/*
Write a function that reverses a string. The input string is given as an array of characters
Input: ["h","e","l","l","o"]
Output: ["o","l","l","e","h"]
*/

function reverseString(str) {
  return str.reverse().join('');
}
console.log(reverseString(["h","e","l","l","o"]))