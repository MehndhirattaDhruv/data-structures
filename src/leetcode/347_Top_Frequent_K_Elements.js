/**
 * Given an integer array nums and an integer k, return the k most frequent elements. You may return the answer in any order.

Example 1:

Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]
Example 2:

Input: nums = [1], k = 1
Output: [1]

**/
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */

// 1st solution
var topKFrequent = function (nums, k) {
  let hash = {};
  for (let i = 0; i < nums.length; i++) {
    if (!hash[nums[i]]) hash[nums[i]] = 0;
    ++hash[nums[i]];
  }

  let slicedArray = Object.values(hash)
    .sort((a, b) => b - a)
    .slice(0, k);
  let result = [];
  for (let key in hash) {
    let index = slicedArray.indexOf(hash[key]);
    if (index !== -1) {
      result.push(Number(key));
      slicedArray.splice(index, 1);
      delete hash[key];
      if (result.length === k) return result;
    }
  }
};




// 2nd solution
/** in this after hash we are creating an array with storing the frequency value as an index
 and its key as value in the array so in the highest index we have max frequency stored
  always and in that we are storing an array of key that belong to that frequency
for example [1,1,1,2,2,3], k = 2
 it will store [empty, [3], [2], [1]] -> ab sbse higest index jo ki 3rd hai mtlb 3 baar 1 aaya hai
 fir 2 baar 2 aaya hai agar koi aur b 2 baar aaya hoga toh wo append hojaega 2 ke sath
*/
/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
 var topKFrequent = function(nums, k) {
  let hash = {};
  let result = [];
  let finalResult = [];
  for(let i = 0; i< nums.length; i++){
      if(!hash[nums[i]]) hash[nums[i]] = 0;
      ++hash[nums[i]];
  }

  for(let key in hash){
      if(!result[hash[key]]) {
          result[hash[key]] = []
      }
      result[hash[key]].push(key);
  }
  for(let i = result.length - 1; i > 0; i--){
      if(Array.isArray(result[i]) && result[i].length){
           result[i].forEach((item) => {
               finalResult.push(Number(item));
           });
          if(finalResult.length === k) return finalResult
      }
  }
};

// 3rd solution using sorting kind of 1st one but using Map in javascript which can be iterable

/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number[]}
 */
 var topKFrequent = function(nums, k) {
  let hash = new Map();
 let result = [];
 let finalResult = [];
 for(let i = 0; i< nums.length; i++){
     if(!hash.has(nums[i])) hash.set(nums[i], 0);
     hash.set(nums[i], hash.get(nums[i]) + 1)
 }
 let arr = [...hash];
 arr = arr.sort((a,b) => b[1] - a[1]);
 return arr.slice(0, k).map((item) => item[0])
};