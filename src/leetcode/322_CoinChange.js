/**
 You are given an integer array coins representing coins of different denominations and an integer amount representing a total amount of money.

Return the fewest number of coins that you need to make up that amount. If that amount of money cannot be made up by any combination
 of the coins, return -1.

You may assume that you have an infinite number of each kind of coin.

Example 1:

Input: coins = [1,2,5], amount = 11
Output: 3
Explanation: 11 = 5 + 5 + 1
Example 2:

Input: coins = [2], amount = 3
Output: -1
Example 3:

Input: coins = [1], amount = 0
Output: 0
 */
/**
 * @param {number[]} coins
 * @param {number} amount
 * @return {number}
 */
var coinChange = function (coins, amount) {
  let minCoinsDp = new Array(amount + 1).fill(Infinity);
  minCoinsDp[0] = 0;
  for (let i = 0; i < minCoinsDp.length; i++) {
    for (let j = 0; j < coins.length; j++) {
      if (i >= coins[j]) {
        minCoinsDp[i] = Math.min(minCoinsDp[i], minCoinsDp[i - coins[j]] + 1);
      }
    }
  }
  const lastItem = minCoinsDp.pop();
  return lastItem === Infinity ? -1 : lastItem;
};


/**
 * explanation

 * 1,2,5 -> 11
            0 1 2 3 4 5 6 7 8 9 10 11
            0 ∞ ∞ ∞ ∞ ∞ ∞ ∞ ∞ ∞  ∞  ∞
 for 1
            0 1 2 3 4 5 6 7 8 9 10 11
it implies we need 11 coins of 1 rupee to make amount 11

for 2 coin we  not need to change if the index is less than the coin because index represent amount itself
 and if its equal or greater than that than start finding min from the  starting by index - coin value + 1 or existing value
            0 1 1 2 2 3 3 4 4 5 5 6
for coin 5
            0 1  1 2 2 1 2 2 3 3 2 3

check if final value is the Infinity return -1 else return the popped value
 */