/*
Given an array nums,
write a function to move all 0's to the end of it while
maintaining the relative order of the non-zero elements
Input: [0,1,0,3,12]
Output: [1,3,12,0,0]

*/

function moveZeroes(arr) {
  return [...arr.filter((item) => item), ...arr.filter((item) => !item)];
}
console.log(moveZeroes([0, 1, 0, 3, 12]));