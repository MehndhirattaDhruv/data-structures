// this function takes two arguments n as number and input as array
// find the majority element which occurs more than half of n i.e n/2
// if there is no return -1

function returnMajorityNumber(n, input) {
  let result = {};
  input.forEach((item) => {
    if (!result[item]) {
      result[item] = 0;
    }
    ++result[item];
  });

  let threshold = n / 2;
  let output = -1;
  for (var key in result) {
    if (result[key] > threshold) output = key;
  }
  return Number(output);
}

console.log(returnMajorityNumber(3, [1, 2, 1]));
// output 1 because n/2 is 1.5 and 1 ocuurs 2 times so we expect the key to be returned which has value greater than 1.5

console.log(returnMajorityNumber(4, [1, 2, 1, 2]));
//output -1 because n/2 is 2 and 1 ocuurs 2 times and 2 also occurs two times but since no one is greater than n/2 we need to return -1
