// this function will help converting camelcase string to snake case and vice versa

function convertStringType(str) {
  let arr = str.split("");
  let output = [];
  // convert snakecase to camelcase
  if (str.indexOf("_") != -1) {
    let counter = null;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] === "_") {
        counter = i + 1;
      } else if (i === counter) {
        output.push(arr[i].toUpperCase());
      } else {
        output.push(arr[i]);
      }
    }
    return output.join("");
  }

  // convert camelCase to snake_case
  else {
    return str.replace(/[A-Z]/g, function (m) {
      return "_" + m.toLowerCase();
    });
  }
}

console.log(convertStringType("my_variable_dummy_variable"));
console.log(convertStringType("myVariableDummyStuff"));
