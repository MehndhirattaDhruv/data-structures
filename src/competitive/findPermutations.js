function findPermutations(str) {
  let result = [];

  if (str.length === 1) return [str];

  for (let i = 0; i < str.length; i++) {
    let ch = str.charAt(i);

    let rem = str.substring(0, i) + str.substring(i + 1, str.length);

    result.push(...findPermutations(rem).map((item) => ch + item));
  }
  return result;
}

console.log(findPermutations("let"));



