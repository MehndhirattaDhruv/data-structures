/**
 * @param {string} s
 * @return {string}
 */
var longestPalindrome = function (s) {
  let palindromeString = "";
  for (let i = 0; i < s.length; i++) {
    if (palindromeString.length > s.length - i) break;
    for (let j = s.length; j >= i; j--) {
      let checkingString = s.substring(i, j);
      if (
        isPalindrome(checkingString) &&
        checkingString.length > palindromeString.length
      ) {
        palindromeString = checkingString;
      }
    }
  }
  return palindromeString;
};

const isPalindrome = (str) => {
  if (!str) return false;
  for (let i = 0; i < Math.floor(str.length / 2); i++) {
    if (str[i] !== str[str.length - 1 - i]) {
      return false;
    }
  }
  return true;
};
