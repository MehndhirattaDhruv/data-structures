/*
First element occurring k times in an array
Given an array of n integers. The task is to find the first element that occurs k number of times.
If no element occurs k times the print -1. The distribution of integer elements could be in any range.

Input: {1, 7, 4, 3, 4, 8, 7},
k = 2
Output: 7
Both 7 and 4 occur 2 times.
But 7 is the first that occurs 2 times.

Input: {4, 1, 6, 1, 6, 4},
k = 3
Output: -1

*/


function firstElementOccuring(arr, k) {
  let result = {};
  for (let i = 0; i < arr.length; i++) {
    if (!result[arr[i]])
      result[arr[i]] = 0;
    ++result[arr[i]];
    if (result[arr[i]] === k) return arr[i];
  }
  return -1;
}

console.log(firstElementOccuring([1, 7, 4, 3, 4, 8, 7], 2))
