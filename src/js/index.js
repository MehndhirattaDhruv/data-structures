// prototype inheritance
let car = function(model) {
  this.model = model
}

car.prototype.getModel = function() {
  return this.model;
}

const bmw = new car("bmw");
console.log(bmw.getModel())

const nisan = new car("nisan");
console.log(nisan.getModel())


// closure
/*
when the returned function from a function holds its environment and variable
in its closure scope then we can say that internal function is using values from its closures
*/
let func = function() {
  let i = 1;
  return {
    setI(k) {
      i = k
    },
    getI(){
      return i;
    } 
  }
}
let obj = func()
obj.setI(4);
obj.getI()


/**
 * cs specificity rules
 1) if same element tag has two css then it will pick the later one
 <div>name</div>
 css part
 div {background-color: red}
  div {background-color: green}
  2) if css is given by class then it will pick that up that class one irrespective to order because class is more specific
  <div class="demo">name</div>
   div {background-color: red}
  div.demo {background-color: green}
  3) if we have id then it also overide the class also and will show yellow color 
   <div class="demo" id="dummy">name</div>
    div.demo {background-color: green}
    div#dummy {background-color: yellow}
    but if we give important to the class then it will also override the id
 */

 /**
  * pseudo classes are hover, focus etc :hover, :focus
  * and pseudoe elememts are ::after, ::before, ::firstline etc
  * p:hover::after{ content: 'this is a tooltip content'}
  */

  /**
   * if in html we want to store data we can use data attributes because normal
   * attributes are not suitable for storing data for eg src attribute in img have different meaning
   * for example
   * <div class="profile" data-content="hello its a a tooltip" data-id="1">demo</div>
   * in css we can use these values
   * .profile:hover::after { content: attr(data-content)}
   */

   /**
    *
    * doubt
      let f;

      if(true) {
      let i =1;
      f = () => {
      console.log(i)
      }
      }

      console.log(f())
    */