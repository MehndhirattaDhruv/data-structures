let values = [
  {
    name: 'dhruv',
    age: 12,
    address: '123'
  },
  {
    name: 'divya',
    age: 24,
    address: 'test'
  },
  {
    name: 'dsfas',
    address: 'sf',
    age: 88,
  },
  {
    name: 'ram',
    age: 45,
    address: 'abc'
  }
]

/*
upcoming
1) age twice using map as well as foreach but will return the whole object
2) use destructuring in map and foreach
3) references array of objects

*/
/**
  every->>
  name ki string ki length kya wo greater than 3

 const output = values.every((i) => {
  return i.name.length >= 3;
 })

  console.log(output)



some->>
age ki value 70 se jda h to btao

const output = values.some((i) => {
  return i.age > 70;
})

console.log(output)

**/



/*

const item = values.find((entity, i) => entity.age === 24) // undefined(agar condition na match hui toh)
const index = values.findIndex((entity, i) => entity.age === 24) // -1(agar kuch ni mila toh)

if (index !== -1) {
  let key = 'name'
  values[index][key] = "parveen"
}


console.log(values)


const filteredValues = values.filter((i) => i.age > 15)


console.log(filteredValues);





const response = values.map((e) => e.name)


console.log(response);


map
filter
find
findIndex

*/
