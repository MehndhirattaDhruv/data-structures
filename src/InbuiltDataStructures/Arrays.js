/*
  Arrays are ordered data structure in which whatever we are pushing at the starting is maintained at the start or 1st index
  so its a kind of ordered data structure
  they are not the only ordered data structure available on earth we have single linked list or double linked list
  but dealing with array's is quite simple
  eg const values = ['v', 'b', 'z'];
  Insertion in Array - Depends on which position you want to insert
   - if we are pushing at the end of the array its constant time O(1)
   - if we are trying to insert to beginning we need to re index all the elements in the array - O(n)
  Deletion in Array - Depends on which position you want to insert
   - if we are removing at the end of the array its constant time O(1)
   - if we are trying to remove from beginning we need to re index all the elements in the array - O(n)
  Searching in Array - O(n)
  Accessing in array - O(1) eg values[0] we need index for accessing

  push and pop are always efficient because they perform the operation at the last
  and shift and unshift always costlier operations
  shift removes the first element of the array
  const fruits = ["Banana", "Orange", "Apple", "Mango"];
  fruits.unshift("Lemon","Pineapple");
  whereas unshift add the elements at the beginning
  push -  O(1)
  pop -  O(1)
  shift - O(n)
  unshift - O(n)
  concat - O(n)
  slice - O(n)
  splice - O(n)
  sort - O(n * logn)
  forEach/Map/Filter/Reduce - O(n)
*/

/*
Sorted order of best time complexity to worst
- O(1)
- O(logn)
- O(n)
- O(n * log n)
- O(n * n) or n square

*/