/*
  Objects are un ordered data structure which has key value pair
  Big O notation terms is mostly O(1) because it using hashing principle
    - Insertion O(1)
    - Deletion O(1)
    - Accessing O(1)
    - Searching O(n)
    eg var obj ={
      firstname :'dhruv',
      lastname: 'mehndhiratta',
      age: 12
    }
    searching here is not mean accessing the keys we already discussed that access can be done via O(1) but searching here means
    if we want to search for a value like 12 of age then we need to iterate through whole object and check where it exists

    some built in methods
    Object.keys = O(n) // as it depends on the length of the object
    Object.values = O(n)
    Object.entries = O(n)
    Object.hasOwnProperty = O(1)

    bottom line they are faster but unordered
*/