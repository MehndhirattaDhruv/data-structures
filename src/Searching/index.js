/**
 * linear search is best suited for the unsorted array infact js is already using the
 * linear search for indexOf, find, findIndex, includes and its having complexity O(n)
 */
function linearSearch(arr, value) {
  for(let i = 0; i< arr1.length; i++) {
    if(arr[i] === value) return i;
  }
  return -1;
}

linearSearch([1,5,2,4,11,14,7], 18)

/*
* Binary search only works for sorted array and its also called divide and conquer
 */

const binarySearch = (arr, value) => {
  let beg = 0;
  let end = arr.length -1;
  let mid = Math.floor((beg + end)/2);
    if(arr[mid] == value) return mid;
    while(arr[mid] !== value && beg <= end) {
      mid = Math.floor((beg + end)/2);
      if(arr[mid] === value) return mid
      else if(arr[mid] < value) {
        beg = mid + 1;
      }else {
        end = mid - 1;
      }
    }
    return -1;
  }

  /**
   * naive search where we are going to find whether a substring is existing
   * in the string or not and returns the count how many times its existing
   */
  function stringSearch(str, pattern) {
    let counter = 0;
     for(let i =0; i < str.length; i++) {
       for(let j = 0; j < pattern.length; j++) {
         if(str[i+j] !== pattern[j]){
           break;
         }
         else if(j === pattern.length - 1){
           counter++;
         }
       }
     }
    return counter;
  }

  console.log(stringSearch("wegnomgtle", "omg")); // 1
  console.log(stringSearch("wegnomgtleomg", "omg")) // 2
  console.log(stringSearch("tensnenomt", "omg")); // 0