export function isValidEmail(email) {
  const tester =
    /^[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/;

  return tester.test(email);
}

export function isAllLetters(char) {
  if (typeof char !== 'string') {
    return false;
  }

  return /^[a-zA-Z]+$/.test(char);
}

/**
 *
 * Add the following validation to the user object:

The username property has to be a string that only contains of letters, and is at least 3 characters long
The email property has to be a valid email address.
The age property has to be a number, and has to be at least 18
When a property is retrieved, change the output to ${new Date()} | The value of ${property}} is ${target[property]}.
 For example if we get user.name, it needs to log 2022-05-31T15:29:15.303Z | The value of name is John

 */

var user = {
  firstName: 'John',
  lastName: 'Doe',
  username: 'johndoe',
  age: 42,
  email: 'john@doe.com',
}

var userProxy = new Proxy(user, {
  get: (target, property) => {
   return `${new Date()} | The value of ${property} is ${target[property]}`;
  },
  set: (target, property, value) => {
    if(property === 'email'){
      if(!isValidEmail(value)){
       console.log('please provide a valid email...')
       return false
      }
    }
    if(property === 'username'){
      if (value.length < 3) {
        throw new Error('Please use a longer username.');
      } else if (!isAllLetters(value)) {
        throw new Error('You can only use letters');
      }
    }
    if(property === 'age'){
      if (typeof value !== 'number') {
        throw new Error('Please provide a valid age.');
      }

      if (value < ex18) {
        throw 'User cannot be younger than 18.';
      }
    }
    return target[property] = value;
  }
})