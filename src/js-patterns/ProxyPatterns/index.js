/**
 var person = {
  name: 'dhruv',
  age: 2
 }
 * In javascript if we want to update any property in the object we directly access that
 like person.age += 1
 but in case of proxy it has getter and setter that will always do the update
 */

 const person = {
  name: 'dhruv',
  age: 12,
  email: 'ad@ds.com'
 }

 const personProxy = new Proxy(person, {
  // here target will automatically use person as target argument or 1st argument
  get: (target, property) => {
    console.log(`the value of the ${property} is ${target[property]}`)
    return target[property];
  },
  set: (target, property, value) => {
    console.log(`changed ${property} from ${target[property]} to ${value}`)
    target[property] = value;
    return true
  }
 })

 console.log(personProxy.name) // dhruv
 personProxy.age = 13
 console.log(personProxy.age)


 /**
  * advantages
  * if we want to maintain the log the long handler execution or need to add logs to the queue
    like from what has changed to what then this will be the best pattern because every set ant get will go through only this function its
    super easy to debug this things by this pattern
  * proxies make it easy to add functionality like interacting with a certain object, such as validation, logging, formatting, notifications, debugging
  */
 /**
  * only drawback is whenever we are interacting with the target these methods will execute and sending the data/analytics might take some time in order
    to solve that we can use async approach of js
  */