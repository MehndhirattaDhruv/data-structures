/**
 * both of the ways can be used for singleton either class based or object(es6) based
 * in both of them we are initializing the instance one time and then sharing that initiated instance
 * with the outer world for usage, and any change in that can impact the created instance because its shared instance
 */

/**
 * Pros
 * memory savage is there since only one instance is shared across application
 */

/**
 * Cons
 * es6 are by default singleton we don't need classes
 * global scope pollution: since its shared globally accidently over riding this kind of variable lead to unexpected behavior
 or we can see its a memory leak since its going to be access through the window object or its globally accessible

 */

/**
 * usage in case of mongo db class which has connect and disconnect methods
 * we want them to have only single instance throughout application otherwise it can lead to multiple instance to db
 * which is very bad
 */