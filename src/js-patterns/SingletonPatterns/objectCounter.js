let counter = 0;
export default Object.freeze({
  increment: () => ++counter,
  decrement: () => --counter,
  getCount: () => counter
})