let instance;
class Counter {
  constructor(counter){
    if(instance){
      throw new Error('Instance already exist')
    }
    this.counter = counter;
    instance = this;
  }
  increment(){
    return this.counter++;
  }
  decrement(){
    return this.counter--;
  }
  getCounter(){
    return this.counter
  }
}

const singletonCounter = Object.freeze(new Counter(0))

export default singletonCounter;


